/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.marvelapp;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.marvelapp";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String BASE_URL = "https://gateway.marvel.com/v1/public/";
  public static final String MarvelPrivateKey = "867e099f7701dd1fa19be0b4933f8fbc9896a587";
  public static final String MarvelPublicKey = "f679b123477e9460ff692f596fcd91c2";
}
