package com.example.marvelapp.ui.heroes.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import com.example.marvelapp.R;
import com.example.marvelapp.domain.model.Result;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class HeroesFragmentDirections {
  private HeroesFragmentDirections() {
  }

  @NonNull
  public static NavigateToHeroesDetail navigateToHeroesDetail(@NonNull Result hero) {
    return new NavigateToHeroesDetail(hero);
  }

  public static class NavigateToHeroesDetail implements NavDirections {
    private final HashMap arguments = new HashMap();

    private NavigateToHeroesDetail(@NonNull Result hero) {
      if (hero == null) {
        throw new IllegalArgumentException("Argument \"hero\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("hero", hero);
    }

    @NonNull
    public NavigateToHeroesDetail setHero(@NonNull Result hero) {
      if (hero == null) {
        throw new IllegalArgumentException("Argument \"hero\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("hero", hero);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("hero")) {
        Result hero = (Result) arguments.get("hero");
        if (Parcelable.class.isAssignableFrom(Result.class) || hero == null) {
          __result.putParcelable("hero", Parcelable.class.cast(hero));
        } else if (Serializable.class.isAssignableFrom(Result.class)) {
          __result.putSerializable("hero", Serializable.class.cast(hero));
        } else {
          throw new UnsupportedOperationException(Result.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.navigate_to_heroes_detail;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public Result getHero() {
      return (Result) arguments.get("hero");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      NavigateToHeroesDetail that = (NavigateToHeroesDetail) object;
      if (arguments.containsKey("hero") != that.arguments.containsKey("hero")) {
        return false;
      }
      if (getHero() != null ? !getHero().equals(that.getHero()) : that.getHero() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getHero() != null ? getHero().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "NavigateToHeroesDetail(actionId=" + getActionId() + "){"
          + "hero=" + getHero()
          + "}";
    }
  }
}
