1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.example.marvelapp"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="26"
8-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:5:5-67
11-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:5:22-64
12
13    <application
13-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:7:5-24:19
14        android:name="com.example.marvelapp.heroesApplication"
14-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:12:9-42
15        android:allowBackup="true"
15-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:8:9-35
16        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
16-->[androidx.core:core:1.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/1e15b5805e6418e2a0f27ad905a7db80/core-1.2.0/AndroidManifest.xml:24:18-86
17        android:debuggable="true"
18        android:icon="@mipmap/ic_launcher"
18-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:9:9-43
19        android:label="@string/app_name"
19-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:11:9-41
20        android:roundIcon="@mipmap/ic_launcher_round"
20-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:10:9-54
21        android:supportsRtl="true"
21-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:13:9-35
22        android:testOnly="true"
23        android:theme="@style/NoActionBar"
23-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:15:9-43
24        android:usesCleartextTraffic="true" >
24-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:14:9-44
25        <activity
25-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:16:9-22:37
26            android:name="com.example.marvelapp.ui.splashscreen.SplashScreenActivity"
26-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:16:19-71
27            android:theme="@style/AppCompat.TelaCheia" >
27-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:17:13-55
28            <intent-filter>
28-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:18:13-22:25
29                <action android:name="android.intent.action.MAIN" />
29-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:19:13-65
29-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:19:21-62
30
31                <category android:name="android.intent.category.LAUNCHER" />
31-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:21:13-73
31-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:21:23-70
32            </intent-filter>
33        </activity>
34        <activity android:name="com.example.marvelapp.ui.heroes.activity.HeroesActivity" />
34-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:23:9-80
34-->/Users/luisfilipe/StudioProjects/marvelApp/app/src/main/AndroidManifest.xml:23:19-68
35
36        <provider
36-->[com.squareup.picasso:picasso:2.71828] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/3b82462487340e9fb5e28b80cc9cae90/jetified-picasso-2.71828/AndroidManifest.xml:8:9-11:40
37            android:name="com.squareup.picasso.PicassoProvider"
37-->[com.squareup.picasso:picasso:2.71828] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/3b82462487340e9fb5e28b80cc9cae90/jetified-picasso-2.71828/AndroidManifest.xml:9:13-64
38            android:authorities="com.example.marvelapp.com.squareup.picasso"
38-->[com.squareup.picasso:picasso:2.71828] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/3b82462487340e9fb5e28b80cc9cae90/jetified-picasso-2.71828/AndroidManifest.xml:10:13-72
39            android:exported="false" />
39-->[com.squareup.picasso:picasso:2.71828] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/3b82462487340e9fb5e28b80cc9cae90/jetified-picasso-2.71828/AndroidManifest.xml:11:13-37
40        <provider
40-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/044e48ebf1b1ec340fca8c9677cbaffe/lifecycle-process-2.2.0/AndroidManifest.xml:25:9-29:43
41            android:name="androidx.lifecycle.ProcessLifecycleOwnerInitializer"
41-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/044e48ebf1b1ec340fca8c9677cbaffe/lifecycle-process-2.2.0/AndroidManifest.xml:26:13-79
42            android:authorities="com.example.marvelapp.lifecycle-process"
42-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/044e48ebf1b1ec340fca8c9677cbaffe/lifecycle-process-2.2.0/AndroidManifest.xml:27:13-69
43            android:exported="false"
43-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/044e48ebf1b1ec340fca8c9677cbaffe/lifecycle-process-2.2.0/AndroidManifest.xml:28:13-37
44            android:multiprocess="true" />
44-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/luisfilipe/.gradle/caches/transforms-2/files-2.1/044e48ebf1b1ec340fca8c9677cbaffe/lifecycle-process-2.2.0/AndroidManifest.xml:29:13-40
45    </application>
46
47</manifest>
